package kz.aitu.week3.Week4.Queue;

import kz.aitu.week3.Week4.Node4;

public class QueueNode {
    public QueueNode next;
    public String data;

    public QueueNode(String data){
        this.data = data;
    }

    public void setNext(QueueNode next) {
        this.next = next;
    }

    public String toString(){
        return this.data;
    }
}

